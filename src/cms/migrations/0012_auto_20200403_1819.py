# Generated by Django 3.0.5 on 2020-04-03 18:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0011_site_instagram'),
    ]

    operations = [
        migrations.AddField(
            model_name='therapist',
            name='publish',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='therapy',
            name='publish',
            field=models.BooleanField(default=True),
        ),
    ]
