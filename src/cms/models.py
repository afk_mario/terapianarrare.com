#!/usr/bin/env python

from adminsortable.models import SortableMixin
from ckeditor.fields import RichTextField
from colorfield.fields import ColorField
from django.db import models
from django.template import defaultfilters
from solo.models import SingletonModel


def upload_to(instance, filename):
    import os

    from django.utils.timezone import now

    filename_base, filename_ext = os.path.splitext(filename)
    return "uploads/{}{}{}".format(
        filename_base,
        now().strftime("%Y%m%d%H%M%S"),
        filename_ext.lower(),
    )


def upload_to_therapy(instance, filename):
    import os.path

    filename_base, filename_ext = os.path.splitext(filename)
    return "therapy/{}{}".format(
        instance.slug,
        filename_ext.lower(),
    )


def upload_to_therapist(instance, filename):
    import os.path

    filename_base, filename_ext = os.path.splitext(filename)
    return "therapist/{}{}".format(
        instance.slug,
        filename_ext.lower(),
    )


class Site(SingletonModel):
    title = models.CharField(max_length=140, default="Illic")
    social_description = models.CharField(max_length=140, default="Social description")
    preview_image = models.ImageField(upload_to=upload_to)
    who = RichTextField("¿Quiénes somos?")
    who_image = models.ImageField(upload_to=upload_to)
    when = RichTextField("¿Cuándo acudir a Terapia")
    when_image = models.ImageField(upload_to=upload_to)
    email = models.EmailField(max_length=254)
    message = RichTextField("Agendar una cita")
    facebook = models.URLField(max_length=140, default="https://www.facebook.com/")
    instagram = models.URLField(max_length=140, default="https://www.instagram.com/")
    twitter = models.URLField(max_length=140, default="https://twitter.com/")
    footer_message = models.CharField(max_length=140)
    mapAddress = models.TextField(
        "Mapa",
        help_text="En google maps escoger la opción de compartir, Embed Map y copiar el url dentro de src=URL",
    )
    address = RichTextField("Dirección")

    def __str__(self):
        return "Textos del sitio"

    class Meta:
        verbose_name = "Textos del sitio"
        verbose_name_plural = "Textos del sitio"


class Therapy(SortableMixin):
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    publish = models.BooleanField(default=True)
    color01 = ColorField(default="#d17be9")
    color02 = ColorField(default="#79caec")
    title = models.CharField(max_length=140, default="Terapia")
    image = models.FileField(upload_to=upload_to_therapy)
    large_image = models.FileField(upload_to=upload_to)
    small_text = models.CharField(max_length=140, default="Terapia")
    text = RichTextField()
    slug = models.CharField(max_length=200, editable=False)

    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.title)
        if self.pk is None:
            saved_image = self.image
            self.image = None
            super().save(*args, **kwargs)
            self.image = saved_image
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Terapia"
        verbose_name_plural = "Terapias"
        ordering = ["order"]


class Therapist(SortableMixin):
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    publish = models.BooleanField(default=True)
    title = models.CharField("Nombre", max_length=140, default="Terapeuta")
    image = models.ImageField(upload_to=upload_to_therapist)
    slug = models.CharField(max_length=200, editable=False)
    phone = models.CharField(max_length=140)

    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.title)
        if self.pk is None:
            saved_image = self.image
            self.image = None
            super().save(*args, **kwargs)
            self.image = saved_image
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Terapeuta"
        verbose_name_plural = "Terapeutas"
        ordering = ["order"]
