from django.shortcuts import render

from blog.models import Entry

from .models import Site, Therapist, Therapy

DEFAULT_TITLE = ""
DEFAULT_DESCRIPTION = ""
DEFAULT_PREVIEW = ""


def home(request):
    single = Site.get_solo()
    latest_blog = Entry.objects.latest("dateCreated")
    list_therapy = Therapy.objects.filter(publish=True)
    list_therapist = Therapist.objects.filter(publish=True)
    context = {
        "title": single.title,
        "description": single.social_description,
        "preview": single.preview_image.url,
        "list": list_therapy,
        "list_therapist": list_therapist,
        "latest_blog": latest_blog,
        "single": single,
    }

    return render(request, "cms/home.html", context)
