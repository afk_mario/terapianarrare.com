from django.contrib import admin
from django.utils.safestring import mark_safe
from solo.admin import SingletonModelAdmin

from .models import Site, Therapist, Therapy


class AdminImageMixin:
    @mark_safe
    def admin_image(self, obj):
        url = obj.image.url
        return f"""
            <img
                src='{url}'
                style='height: 100px; width: auto; display: block'
            />
        """

    admin_image.allow_tags = True
    admin_image.short_description = "Preview"


@admin.register(Site)
class SiteAdmin(SingletonModelAdmin):

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "title",
                    "preview_image",
                    "social_description",
                    "footer_message",
                )
            },
        ),
        (
            "¿Cuándo acudir a Terapia?",
            {
                "fields": (
                    "when_image",
                    "when",
                )
            },
        ),
        (
            "¿Quiénes  Somos?",
            {
                "fields": (
                    "who_image",
                    "who",
                )
            },
        ),
        (
            "Contact",
            {
                "fields": (
                    "email",
                    "message",
                    "mapAddress",
                    "address",
                )
            },
        ),
        (
            "Social",
            {
                "fields": (
                    "facebook",
                    "twitter",
                    "instagram",
                )
            },
        ),
    )


@admin.register(Therapy)
class TherapyAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = (
        "publish",
        "title",
        "small_text",
        "admin_image",
    )
    list_editable = ("publish",)
    list_display_links = (
        "title",
        "small_text",
        "admin_image",
    )


@admin.register(Therapist)
class TherapistAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = (
        "publish",
        "title",
        "phone",
        "admin_image",
    )
    list_editable = ("publish",)
    list_display_links = (
        "title",
        "phone",
        "admin_image",
    )
