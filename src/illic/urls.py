from django.contrib import admin
from django.urls import include, path

from blog import urls
from cms import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("ckeditor/", include("ckeditor_uploader.urls")),
    path("", views.home, name="home"),
    path("__debug__/", include("debug_toolbar.urls")),
    path("blog/", include(urls)),
]
