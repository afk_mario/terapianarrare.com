# from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.urls import reverse
from taggit.managers import TaggableManager
from uuslug import uuslug

from cms.models import Therapist


def upload_to_entry(instance, filename):
    import os.path

    filename_base, filename_ext = os.path.splitext(filename)
    return "blog/entry/{}{}".format(
        instance.slug,
        filename_ext.lower(),
    )


class Entry(models.Model):
    publish = models.BooleanField(default=True)
    title = models.CharField(max_length=100, unique=True)
    author = models.ForeignKey(Therapist, on_delete=models.CASCADE)
    social_description = models.CharField(max_length=140)
    image = models.FileField(upload_to=upload_to_entry, blank=True, null=True)
    slug = models.SlugField(max_length=140, unique=True, editable=False)
    text = RichTextUploadingField(config_name="with_media")
    tags = TaggableManager()
    dateCreated = models.DateTimeField(auto_now_add=True, editable=False)
    dateUpdated = models.DateTimeField(auto_now=True, editable=False)

    def get_absolute_url(self):
        return reverse("entry-detail", kwargs={"slug": self.slug})

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self, slug_field="slug")
        if self.pk is None:
            saved_image = self.image
            self.image = None
            super().save(*args, **kwargs)
            self.image = saved_image
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Blog Entry"
        verbose_name_plural = "Blog Entries"
        ordering = ["-dateCreated"]
