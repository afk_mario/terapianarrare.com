from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Entry


class ViewOnSiteMixin:
    @mark_safe
    def view_on_site_list(self, obj):
        url = obj.get_absolute_url()
        return f"""
            <a
            class='button'
            target='_blank'
            href='{url}'
            style='display: block; white-space: nowrap'
            >view on site</a>
        """

    view_on_site_list.allow_tags = True
    view_on_site_list.short_description = "View on site"


class AdminImageMixin:
    @mark_safe
    def admin_image(self, obj):
        if obj.image:
            url = obj.image.url
            return f"""
                <img
                    src='{url}'
                    style='height: 100px; width: auto; display: block'
                />
            """
        else:
            return "NO IMAGE"

    admin_image.allow_tags = True
    admin_image.short_description = "Preview"


@admin.register(Entry)
class EntryAdmin(ViewOnSiteMixin, admin.ModelAdmin, AdminImageMixin):
    date_hierarchy = "dateCreated"
    list_filter = ("publish", "author")
    view_on_site = True
    list_per_page = 10
    list_display = (
        "publish",
        "title",
        "author",
        "tag_list",
        "dateCreated",
        "admin_image",
        "view_on_site_list",
    )
    list_display_links = (
        "title",
        "tag_list",
        "dateCreated",
        "admin_image",
    )
    list_editable = (
        "author",
        "publish",
    )

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("tags")

    def tag_list(self, obj):
        return ", ".join(o.name for o in obj.tags.all())
