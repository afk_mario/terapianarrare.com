from django.views.generic.detail import DetailView
from django_filters.views import FilterView

from cms.models import Site

from .filters import EntryFilter
from .models import Entry
from .view_mixins import PaginatedFilterViews


class EntryDetailView(DetailView):
    model = Entry
    template_name = "cms/entry_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        single = self.object
        context["title"] = single.title
        context["description"] = single.social_description
        context["preview"] = single.image.url
        return context


class EntryListView(PaginatedFilterViews, FilterView):
    model = Entry
    template_name = "cms/entry_list.html"
    filterset_class = EntryFilter
    queryset = Entry.objects.filter(publish=True)
    paginate_by = 12

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        site = Site.get_solo()
        custom_context = {
            "title": site.title,
            "description": site.social_description,
            "preview": site.preview_image.url,
        }
        return {**context, **custom_context}
